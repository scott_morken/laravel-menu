<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 7:29 AM
 */

return array(
    /*
    |--------------------------------------------------------------------------
	| Menu Configuration
	|--------------------------------------------------------------------------
	|
    | Create and enable your storage repository to handle this array (use the
    | examples provided in Storage\Menu).
    |
    | Each item requires either a url key or an action key (controller@action).
    | Menu items will default to adding children menu items if
    | 1) it has an action key
    | 2) it has a url key AND a base_route key (to find children with similar routes)
    | It is almost always more accurate to use the action so that the router
    | can compare the controller.
    | The key value on each group is used in the repository to do any permission
    | checks you might need.
    |   'home' => array(
    |     0 => array(
    |       'id' => 1,
    |       'parent_id' => 0,
    |       'url' => 'someurl.com/route/index',
    |       'value' => 'Home',
    |       'base_route' => 'route/',
    |       'has_children' => true, //default value
    |       'next_id' => 3, //overrides default value of id ++
    |     ),
    |     1 => array(
    |       'id' => 100,
    |       'parent_id' => 0,
    |       'action' => 'MyController@getIndex',
    |       'value' => 'My Page',
    |       'has_children' => true, //default value - base_route not needed since
    |       //routing information is available from the action
    |     ),
    |   ),
    |   'admin' => array(
    |   ...
    |   )
    |
    */
);