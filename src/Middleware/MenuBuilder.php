<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 6:49 AM
 */

namespace Menu\Middleware;

use Closure;
use Illuminate\Routing\Router;
use Illuminate\Support\Str;
use Menu\MenuException;
use Menu\Storage\MenuRepository;

class MenuBuilder {

    protected $routes = array();

    /**
     * @var MenuRepository
     */
    protected $provider;

    public function __construct(MenuRepository $repository)
    {
        $this->provider = $repository;
        $router = app('router');
        $this->setRoutes($router);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $menuitems = $this->provider->all();
        $this->createMainMenu($menuitems);
        return $next($request);
    }

    /**
     * $items = array(
     *   0 => array(
     *     'id' => 1,
     *     'parent_id' => 0,
     *     'url' => 'someurl.com/route/index',
     *     'value' => 'Home',
     *     'base_route' => 'route/',
     *     'has_children' => true, //default value
     *     'next_id' => 3, //overrides default value of id ++
     *   ),
     *   1 => array(
     *     'id' => 100,
     *     'parent_id' => 0,
     *     'action' => 'MyController@getIndex',
     *     'value' => 'My Page',
     *     'has_children' => true, //default value - base_route not needed since
     *     //routing information is available from the action
     *   )
     * )
     * @param array $items
     */
    public function createMainMenu($items)
    {

        $menu = \Menu::handler('main', array('class' => 'nav navbar-nav'));
        $items = $this->createUnhandledChildren($items);
        $this->hydrateMenu($menu, $items);
    }

    /**
     * @param \MenuHandler $menu
     * @param $items
     * @return mixed
     */
    public function hydrateMenu($menu, $items)
    {
        $menu->hydrate($items, function($children, $item) {

            $method = isset($item['raw']) ? 'raw' : 'add';
            if ($this->checkIsVisible($item)) {
                $params = $this->createParamArray($item);
                $itemObj = call_user_func_array(array($children, $method), $params);
                if (isset($item['activePattern'])) {
                    $itemObj->activePattern($item['activePattern']);
                }
            }
        });
        return $menu;
    }

    protected function checkIsVisible($menuitem)
    {
        if (!$menuitem || (isset($menuitem['visible']) && !$menuitem['visible'])) {
            return false;
        }
        return true;
    }

    protected function createParamArray($item)
    {
        $item = $this->createUrlOnItem($item);
        $addparamlist = array(
            'url' => 'req',
            'value' => null,
            'children' => \Menu::items(Str::slug(isset($item['item_id']) ? $item['item_id'] : ($item['value'] ? $item['value'] : $item['url']))),
            'linkAttributes' => array(),
            'itemAttributes' => array(),
            'itemElement' => array(),
        );
        $rawparamlist = array(
            'raw' => 'req',
            'children' => \Menu::items(),
            'itemAttributes' => array(),
            'itemElement' => array(),
        );
        $params = array();
        $paramlist = isset($item['raw']) ? $rawparamlist : $addparamlist;
        foreach($paramlist as $param => $default) {
            if ($default == 'req' && !isset($item[$param])) {
                throw new \InvalidArgumentException("$param is a required argument.");
            }
            if (isset($item[$param])) {
                $params[$param] = $item[$param];
            }
            else {
                $params[$param] = $default;
            }
        }
        return $params;
    }

    protected function createUrlOnItem($item)
    {
        if (isset($item['url'])) {
            return $item;
        }
        if (isset($item['action'])) {
            try {
                $item['url'] = action($item['action']);
            }
            catch (\Exception $e) {
                throw new MenuException($item['action'] . ' has a problem.');
            }
        }
        if (isset($item['route'])) {
            try {
                $item['url'] = route($item['route']);
            }
            catch (\Exception $e) {
                throw new MenuException($item['route'] . ' has a problem.');
            }
        }
        return $item;
    }

    protected function setRoutes($router)
    {
        $this->routes = $router->getRoutes();
    }

    protected function createUnhandledChildren($items)
    {
        foreach($items as $index => $item) {
            if (!$item) {
                unset($items[$index]);
            }
            if (!isset($item['has_children']) || $item['has_children']) {
                $this->addChildren($items, $item);
            }
        }
        return $items;
    }

    protected function addChildren(&$items, $item)
    {
        $next_id = (isset($item['next_id']) ? $item['next_id'] : $item['id'] + 1);
        if (isset($item['action'])) {
            $items = $this->addChildrenToItemsBy('addChildrenFromController', $items, array(
                $item['action'],
                $item['id'],
                $next_id
            ));
        }
        elseif (isset($item['url']) && isset($item['base_route'])) {
            $items = $this->addChildrenToItemsBy('addChildrenFromRoutes', $items, array(
                $item['base_route'],
                $item['id'],
                $next_id
            ));
        }
    }

    protected function addChildrenToItemsBy($method, $items, $params)
    {
        if (method_exists($this, $method)) {
            $children = call_user_func_array(array($this, $method), $params);
            if ($children) {
                $items = array_merge($items, $children);
            }
        }
        return $items;
    }

    protected function addChildrenFromRoutes($base, $parent_id, $start_id)
    {
        $children = array();
        foreach($this->routes as $route) {
            $uri = $route->uri();
            if (in_array('GET', $route->methods()) && starts_with($uri, $base)) {
                $children[] = array(
                    'id' => $start_id,
                    'parent_id' => $parent_id,
                    'url' => action($route->getActionName()),
                    'value' => $this->getNameFromRoute($route),
                    //'visible' => false,
                );
                $start_id ++;
            }
        }
        return $children;
    }

    protected function addChildrenFromController($controller, $parent_id, $start_id)
    {
        $children = array();
        foreach($this->routes as $route) {
            if (!in_array('GET', $route->methods())) {
                continue;
            }
            $action = $route->getAction();
            $actionName = array_get($action, 'controller', '');
            $controller = $this->addNamespaceToController($action, $controller);
            $expController = explode('@', $controller);
            $expActionName = explode('@', $actionName);
            if ($controller !== $actionName && $expActionName[0] == $expController[0]) {
                $children[] = array(
                    'id' => $start_id,
                    'parent_id' => $parent_id,
                    'url' => action($route->getActionName()),
                    'value' => $this->getNameFromRoute($route),
                    //'visible' => false,
                );
                $start_id ++;
            }
        }
        return $children;
    }

    protected function addNamespaceToController(array $action, $controller)
    {
        $namespace = array_get($action, 'namespace', null);
        $namespace = trim($namespace, '\\');
        $controller = trim($controller, '\\');
        if ($namespace && !starts_with($controller, $namespace)) {
            return $namespace . '\\' . $controller;
        }
        return $controller;
    }

    protected function getActionFromRoute($route)
    {
        $action = $route->getActionName();
        if ($action) {
            $action = last(Str::parseCallback($action, null));
            return Str::lower(str_replace(array('get', 'post', 'patch', 'put', 'delete'), '', $action));
        }
    }

    protected function getNameFromRoute($route)
    {
        $name = $route->getName();
        if ($name) {
            return $name;
        }
        $name = $this->getActionFromRoute($route);
        if ($name !== 'index') {
            return $name;
        }
        return null;
    }
}