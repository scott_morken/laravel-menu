<?php
namespace Menu;

use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected $routes = array();

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config' => config_path() . '/vendor/smorken/menu'
        ], 'config');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerResources();
        $this->setupMenuObject();
        $this->app->bind('smorken.menu', function($app) {
            return config('menu::menu', array());
        });
    }

    protected function setupMenuObject()
    {
        $this->app->singleton('menu', function($app) {
            $menu = new Menu();
            $container = $menu->getContainer();
            $container['url'] = $this->app['url'];
            $container['config'] = $this->app['config'];
            $menu->setContainer($container);
            return $menu;
        });
    }

    protected function registerResources()
    {
        $userconfigfile = config_path() . '/vendor/smorken/menu/config.php';
        $packageconfigfile = __DIR__ . '/../config/config.php';
        $this->registerConfig($packageconfigfile, $userconfigfile, 'menu::config');

        $usermenufile = config_path() . '/vendor/smorken/menu/menu.php';
        $packagemenufile = __DIR__ . '/../config/menu.php';
        $this->registerConfig($packagemenufile, $usermenufile, 'menu::menu');

    }

    protected function registerConfig($packagefile, $userfile, $namespace)
    {
        $config = $this->app['files']->getRequire($packagefile);
        if (file_exists($userfile)) {
            $userconfig = $this->app['files']->getRequire($userfile);
            $config = array_replace_recursive($config, $userconfig);
        }
        $this->app['config']->set($namespace, $config);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('menu', 'smorken.menu');
    }

}
