<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 7:49 AM
 */

namespace Menu\Storage;

use Smorken\Repositories\Storage\AbstractArray;
use Smorken\Repositories\Storage\Contracts\Crud;

class ArrayMenuRepository extends AbstractArray implements MenuRepository, Crud {

    public function __construct($model)
    {
        $model = $this->buildMenuArray($model);
        parent::__construct($model);
    }

    protected function buildMenuArray($model)
    {
        $menuarray = array();
        foreach($model as $group => $menus) {
            if ($this->verifyPermissions($group)) {
                foreach($menus as $i => $menu) {
                    $this->handleModify($menus, $i);
                }
                $menuarray = array_merge($menuarray, $menus);
            }
        }
        return $menuarray;
    }

    /**
     * Checks the permissions for the requested group
     * It creates a method based on studly casing
     * the group and prepends it with 'check'
     * ie. 'home' => 'checkHome', 'admin' => 'checkAdmin',
     * 'foo_bar' => 'checkFooBar'
     * The method must return a boolean
     * @param string $group
     * @return bool
     */
    protected function verifyPermissions($group)
    {
        $permission = true;
        $methodname = 'check' . ucfirst($group);
        if (method_exists($this, $methodname)) {
            $permission = $this->$methodname();
        }
        return $permission;
    }

    protected function handleModify(&$menus, $index)
    {
        $menu = $menus[$index];
        if (isset($menu['modify'])) {
            $methodname = $menu['modify'];
            if (method_exists($this, $methodname)) {
                $menus[$index] = $this->$methodname($menu);
            }
        }
    }

}