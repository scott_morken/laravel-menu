<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/5/15
 * Time: 12:26 PM
 */

namespace Menu\Facades;

use Illuminate\Support\Facades\Facade;

class Menu extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'menu'; }

}